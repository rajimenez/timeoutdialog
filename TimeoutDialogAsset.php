<?php

namespace app\components\idleTimeout;

use yii\web\AssetBundle;

class TimeoutDialogAsset extends AssetBundle
{
	public $sourcePath = '@app/components/idleTimeout/assets';

	public $css = [
	];

	public $js = [
		'jquery-idleTimeout.js',
		'store.min.js',
	];

	public $depends = [
		'yii\jui\JuiAsset',
	];
}