<?php

namespace app\components\idleTimeout;

use yii\web\View;
use yii\base\Widget;
use yii\helpers\Json;

class TimeoutDialog extends Widget
{
	public $options = [];

	/**
	 * @var integer The number of your session timeout (in seconds).
	 * The timeout value minus the countdown value determines how long until
	 * the dialog appears.
	 * Default: 1200 = 20 Minutes
	 */
	public $idleTimeLimit; # Seconds

	/**
	 * @var integer The countdown total time to display the warning dialog
	 * before logout (and optional callback) in seconds.
	 * Default: 10 seconds (js: 180 = 3 Minutes)
	 */
	public $dialogDisplayLimit = 10; # Seconds

	/**
	 * @var string The title message in the dialog box.
	 * Default: 'Session Expiration Warning'
	 */
	public $dialogTitle;

	/**
	 * @var string The session idle dialog message.
	 * Default: 'Because you have been inactive, your session is about to expire.'
	 */
	public $dialogText;

	/**
	 * @var string The dialogTimeRemaining message.
	 * Default: 'Time remaining'
	 */
	public $dialogTimeRemaining;

	/**
	 * @var string The text of the YES button to keep the session alive.
	 * Default: 'Stay Logged In'
	 */
	public $dialogStayLoggedInButton;

	/**
	 * @var string The text of the NO button to kill the session.
	 * Default: 'Log Out Now'
	 */
	public $dialogLogOutNowButton;

	/**
	 * @var string Ping the server at this interval in seconds. 600 = 10 Minutes.
	 * @NOTE: Set to false to disable pings
	 * Default: 600 = 10 Minutes
	 */

	public $sessionKeepAliveTimer; # Seconds

	/**
	 * @var string The url that will perform a GET request to keep the
	 * session alive. This GET expects a 'OK' plain HTTP response.
	 * @NOTE: does not apply if sessionKeepAliveTimer: false
	 * Default: window.location.href
	 */

	public $sessionKeepAliveUrl;

	/**
	 * @var string The url to redirect to on logout.
	 * @NOTE: Set to "redirectUrl: false" to disable redirect
	 * Default: '/logout'
	 */
	public $redirectUrl;

	/**
	 * @var integer The width of the dialog box
	 * Default: 540
	 */
	public $dialogWidth;

	public function run()
	{
		$options = array(
			'idleTimeLimit' => $this->idleTimeLimit,
			'dialogDisplayLimit' => $this->dialogDisplayLimit,
			'dialogTitle' => $this->dialogTitle,
			'dialogText' => $this->dialogText,
			'dialogTimeRemaining' => $this->dialogTimeRemaining,
			'dialogStayLoggedInButton' => $this->dialogStayLoggedInButton,
			'dialogLogOutNowButton' => $this->dialogLogOutNowButton,
			'sessionKeepAliveTimer' => $this->sessionKeepAliveTimer,
			'sessionKeepAliveUrl' => $this->sessionKeepAliveUrl,
			'redirectUrl' => $this->redirectUrl,
			'dialogWidth' => $this->dialogWidth,
		);

		# Update sessionKeepAliveTimer if not set (reason: default value too high)
		if(is_null($options['sessionKeepAliveTimer'])){
			if($this->idleTimeLimit){
				$options['sessionKeepAliveTimer'] = ($this->idleTimeLimit/3);
			}
		}

		foreach ($options as $key => $value) {
			if ($value === null)
				unset($options[$key]);
		}
		$this->options = Json::encode($options);

		$this->registerAssets();

		parent::run();
	}

	/**
	 * Registers required assets and the executing code block with the view
	 */
	protected function registerAssets()
	{
		// register the necessary script files
		TimeoutDialogAsset::register($this->view);
		// prepare and register JavaScript code block
		$js = "$(document).idleTimeout($this->options);";
		$key = __CLASS__ . '#' . $this->id;
		$this->view->registerJs($js, View::POS_READY, $key);
	}

}